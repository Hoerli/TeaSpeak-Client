import {setMenuBarDriver} from "tc-shared/ui/frames/menu-bar";
import {NativeMenuBarDriver} from "../MenuBar";

setMenuBarDriver(new NativeMenuBarDriver());