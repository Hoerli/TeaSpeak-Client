namespace minimist {
    export interface Opts {
        /**
         * A string or array of strings argument names to always treat as strings
         */
        string?: string | string[];

        /**
         * A boolean, string or array of strings to always treat as booleans. If true will treat
         * all double hyphenated arguments without equals signs as boolean (e.g. affects `--foo`, not `-f` or `--foo=bar`)
         */
        boolean?: boolean | string | string[];

        /**
         * An object mapping string names to strings or arrays of string argument names to use as aliases
         */
        alias?: { [key: string]: string | string[] };

        /**
         * An object mapping string argument names to default values
         */
        default?: { [key: string]: any };

        /**
         * When true, populate argv._ with everything after the first non-option
         */
        stopEarly?: boolean;

        /**
         * A function which is invoked with a command line parameter not defined in the opts
         * configuration object. If the function returns false, the unknown option is not added to argv
         */
        unknown?: (arg: string) => boolean;

        /**
         * When true, populate argv._ with everything before the -- and argv['--'] with everything after the --.
         * Note that with -- set, parsing for arguments still stops after the `--`.
         */
        '--'?: boolean;
    }

    export interface ParsedArgs {
        [arg: string]: any;

        /**
         * If opts['--'] is true, populated with everything after the --
         */
        '--'?: string[];

        /**
         * Contains all the arguments that didn't have an option associated with them
         */
        _: string[];
    }
}

const parse = (args: string[], options: minimist.Opts) => {
    options = options || {};

    let flags = { bools : {}, strings : {}, unknownFn: null, allBools: false };

    if (typeof options['unknown'] === 'function') {
        flags.unknownFn = options['unknown'];
    }

    if (typeof options['boolean'] === 'boolean' && options['boolean']) {
        flags.allBools = true;
    } else {
        [].concat(options['boolean']).filter(Boolean).forEach(function (key) {
            flags.bools[key] = true;
        });
    }

    let aliases = {};
    Object.keys(options.alias || {}).forEach(function (key) {
        aliases[key] = [].concat(options.alias[key]);
        aliases[key].forEach(function (x) {
            aliases[x] = [key].concat(aliases[key].filter(function (y) {
                return x !== y;
            }));
        });
    });

    [].concat(options.string).filter(Boolean).forEach(function (key) {
        flags.strings[key] = true;
        if (aliases[key]) {
            flags.strings[aliases[key]] = true;
        }
    });

    let defaults = options['default'] || {};

    let argv = { _ : [] };
    Object.keys(flags.bools).forEach(function (key) {
        setArg(key, defaults[key] === undefined ? false : defaults[key]);
    });

    let notFlags = [];

    if (args.indexOf('--') !== -1) {
        notFlags = args.slice(args.indexOf('--')+1);
        args = args.slice(0, args.indexOf('--'));
    }

    function argDefined(key, arg) {
        return (flags.allBools && /^--[^=]+$/.test(arg)) ||
            flags.strings[key] || flags.bools[key] || aliases[key];
    }

    function setArg (key, val, arg?) {
        if (arg && flags.unknownFn && !argDefined(key, arg)) {
            if (flags.unknownFn(arg) === false) return;
        }

        var value = !flags.strings[key] && isNumber(val)
            ? Number(val) : val
        ;
        setKey(argv, key.split('.'), value);

        (aliases[key] || []).forEach(function (x) {
            setKey(argv, x.split('.'), value);
        });
    }

    function setKey (obj, keys, value) {
        var o = obj;
        keys.slice(0,-1).forEach(function (key) {
            if (o[key] === undefined) o[key] = {};
            o = o[key];
        });

        var key = keys[keys.length - 1];
        if (o[key] === undefined || flags.bools[key] || typeof o[key] === 'boolean') {
            o[key] = value;
        }
        else if (Array.isArray(o[key])) {
            o[key].push(value);
        }
        else {
            o[key] = [ o[key], value ];
        }
    }

    function aliasIsBoolean(key) {
        return aliases[key].some(function (x) {
            return flags.bools[x];
        });
    }

    for (let i = 0; i < args.length; i++) {
        let arg = args[i];

        if (/^--.+=/.test(arg)) {
            // Using [\s\S] instead of . because js doesn't support the
            // 'dotall' regex modifier. See:
            // http://stackoverflow.com/a/1068308/13216
            let m = arg.match(/^--([^=]+)=([\s\S]*)$/);
            let key = m[1];
            let value: any = m[2];
            if (flags.bools[key]) {
                value = value !== 'false';
            }
            setArg(key, value, arg);
        }
        /*
        else if (/^--no-.+/.test(arg)) {
            var key = arg.match(/^--no-(.+)/)[1];
            setArg(key, false, arg);
        }
        */
        else if (/^--.+/.test(arg)) {
            let key = arg.match(/^--(.+)/)[1];
            let next = args[i + 1];
            if (next !== undefined && !/^-/.test(next)
                && !flags.bools[key]
                && !flags.allBools
                && (aliases[key] ? !aliasIsBoolean(key) : true)) {
                setArg(key, next, arg);
                i++;
            }
            else if (/^(true|false)$/.test(next)) {
                setArg(key, next === 'true', arg);
                i++;
            }
            else {
                setArg(key, flags.strings[key] ? '' : true, arg);
            }
        }
        else if (/^-[^-]+/.test(arg)) {
            let letters = arg.slice(1,-1).split('');

            let broken = false;
            for (let j = 0; j < letters.length; j++) {
                let next = arg.slice(j+2);

                if (next === '-') {
                    setArg(letters[j], next, arg)
                    continue;
                }

                if (/[A-Za-z]/.test(letters[j]) && /=/.test(next)) {
                    setArg(letters[j], next.split('=')[1], arg);
                    broken = true;
                    break;
                }

                if (/[A-Za-z]/.test(letters[j])
                    && /-?\d+(\.\d*)?(e-?\d+)?$/.test(next)) {
                    setArg(letters[j], next, arg);
                    broken = true;
                    break;
                }

                if (letters[j+1] && letters[j+1].match(/\W/)) {
                    setArg(letters[j], arg.slice(j+2), arg);
                    broken = true;
                    break;
                }
                else {
                    setArg(letters[j], flags.strings[letters[j]] ? '' : true, arg);
                }
            }

            let key = arg.slice(-1)[0];
            if (!broken && key !== '-') {
                if (args[i+1] && !/^(-|--)[^-]/.test(args[i+1])
                    && !flags.bools[key]
                    && (aliases[key] ? !aliasIsBoolean(key) : true)) {
                    setArg(key, args[i+1], arg);
                    i++;
                }
                else if (args[i+1] && /true|false/.test(args[i+1])) {
                    setArg(key, args[i+1] === 'true', arg);
                    i++;
                }
                else {
                    setArg(key, flags.strings[key] ? '' : true, arg);
                }
            }
        }
        else {
            if (!flags.unknownFn || flags.unknownFn(arg) !== false) {
                argv._.push(
                    flags.strings['_'] || !isNumber(arg) ? arg : Number(arg)
                );
            }
            if (options.stopEarly) {
                argv._.push.apply(argv._, args.slice(i + 1));
                break;
            }
        }
    }

    Object.keys(defaults).forEach(function (key) {
        if (!hasKey(argv, key.split('.'))) {
            setKey(argv, key.split('.'), defaults[key]);

            (aliases[key] || []).forEach(function (x) {
                setKey(argv, x.split('.'), defaults[key]);
            });
        }
    });

    if (options['--']) {
        argv['--'] = [];
        notFlags.forEach(function(key) {
            argv['--'].push(key);
        });
    }
    else {
        notFlags.forEach(function(key) {
            argv._.push(key);
        });
    }

    return argv;
};

function hasKey (obj, keys) {
    let o = obj;
    keys.slice(0,-1).forEach(function (key) {
        o = (o[key] || {});
    });

    let key = keys[keys.length - 1];
    return key in o;
}

function isNumber (x) {
    if (typeof x === 'number') return true;
    if (/^0x[0-9a-f]+$/i.test(x)) return true;
    return /^[-+]?(?:\d+(?:\.\d*)?|\.\d+)(e[-+]?\d+)?$/.test(x);
}


function minimist<T>(args?: string[], opts?: minimist.Opts): T & minimist.ParsedArgs {
    return parse(args || [], opts) as any;
}
export = minimist;