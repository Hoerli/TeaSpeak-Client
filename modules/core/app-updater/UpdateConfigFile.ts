export interface UpdateConfigFile {
    version: number,
    selectedChannel: string
}

export default UpdateConfigFile;