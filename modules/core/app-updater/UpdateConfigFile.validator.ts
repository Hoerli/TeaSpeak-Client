/* tslint:disable */
// generated by typescript-json-validator
import {inspect} from 'util';
import Ajv = require('ajv');
import UpdateConfigFile from './UpdateConfigFile';
export const ajv = new Ajv({"allErrors":true,"coerceTypes":false,"format":"fast","nullable":true,"unicode":true,"uniqueItems":true,"useDefaults":true});

ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

export {UpdateConfigFile};
export const UpdateConfigFileSchema = {
  "$schema": "http://json-schema.org/draft-07/schema#",
  "defaultProperties": [
  ],
  "properties": {
    "selectedChannel": {
      "type": "string"
    },
    "version": {
      "type": "number"
    }
  },
  "required": [
    "selectedChannel",
    "version"
  ],
  "type": "object"
};
export type ValidateFunction<T> = ((data: unknown) => data is T) & Pick<Ajv.ValidateFunction, 'errors'>
export const isUpdateConfigFile = ajv.compile(UpdateConfigFileSchema) as ValidateFunction<UpdateConfigFile>;
export default function validate(value: unknown): UpdateConfigFile {
  if (isUpdateConfigFile(value)) {
    return value;
  } else {
    throw new Error(
      ajv.errorsText(isUpdateConfigFile.errors!.filter((e: any) => e.keyword !== 'if'), {dataVar: 'UpdateConfigFile'}) +
      '\n\n' +
      inspect(value),
    );
  }
}
