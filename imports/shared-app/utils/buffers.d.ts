export declare function str2ab8(str: any): ArrayBuffer;
export declare function arrayBufferBase64(base64: string): ArrayBuffer;
export declare function base64_encode_ab(source: ArrayBufferLike): string;
