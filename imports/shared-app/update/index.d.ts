import { Updater } from "./Updater";
export declare function setUIUpdater(updater: Updater): void;
export declare function setNativeUpdater(updater: Updater): void;
export declare function checkForUpdatedApp(): void;
