export declare function encode_text(buffer: string): ArrayBuffer;
export declare function sha1(message: string | ArrayBuffer): PromiseLike<ArrayBuffer>;
