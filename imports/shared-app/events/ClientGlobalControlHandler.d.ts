import { Registry } from "../events";
import { ClientGlobalControlEvents } from "../events/GlobalEvents";
export declare function initialize(event_registry: Registry<ClientGlobalControlEvents>): void;
