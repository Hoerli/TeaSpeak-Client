import { EventType } from "tc-shared/connectionlog/Definitions";
export declare function requestWindowFocus(): void;
export declare function isFocusRequestEnabled(type: EventType): any;
