import * as React from "react";
export declare const isYoutubeLink: (text: string) => boolean;
export declare const YoutubeRenderer: (props: {
    children?: React.ReactElement | React.ReactElement[];
    url: string;
}) => JSX.Element;
