/// <reference types="react" />
export declare function isInviteLink(url: string): boolean;
export declare const InviteLinkRenderer: (props: {
    url: string;
    handlerId: string;
}) => JSX.Element;
