/// <reference types="react" />
export declare const SimpleUrlRenderer: (props: {
    target: string;
    children;
}) => JSX.Element;
export declare function fixupJQueryUrlTags(container: JQuery): void;
