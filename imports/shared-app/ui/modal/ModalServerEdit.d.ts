import { ServerEntry, ServerProperties } from "../../tree/Server";
export declare function createServerModal(server: ServerEntry, callback: (properties?: ServerProperties) => Promise<void>): void;
