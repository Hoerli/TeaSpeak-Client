import { ConnectionHandler } from "tc-shared/ConnectionHandler";
export declare function spawnPokeModal(handler: ConnectionHandler, client: {
    clientName: string;
    clientId: number;
    clientUniqueId: string;
}, message: string): void;
