import { ChannelEntry } from "tc-shared/tree/Channel";
import { ServerEntry } from "tc-shared/tree/Server";
export declare function spawnInviteGenerator(target: ChannelEntry | ServerEntry): void;
