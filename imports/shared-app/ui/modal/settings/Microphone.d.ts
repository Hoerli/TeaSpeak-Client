import { Registry } from "tc-shared/events";
import { MicrophoneSettingsEvents } from "tc-shared/ui/modal/settings/MicrophoneDefinitions";
export declare function initialize_audio_microphone_controller(events: Registry<MicrophoneSettingsEvents>): void;
