import { ChangeLog } from "tc-shared/update/ChangeLog";
export declare function spawnUpdatedModal(changes: {
    changesUI?: ChangeLog;
    changesClient?: ChangeLog;
}): void;
