import { Modal } from "../../ui/elements/Modal";
import { TeaSpeakIdentity } from "../../profiles/identities/TeamSpeakIdentity";
export declare function spawnTeamSpeakIdentityImprove(identity: TeaSpeakIdentity, name: string): Modal;
export declare function spawnTeamSpeakIdentityImport(callback: (identity: TeaSpeakIdentity) => any): Modal;
