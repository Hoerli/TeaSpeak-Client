import PermissionType from "../../../permission/PermissionType";
export declare const senseless_server_group_permissions: PermissionType[];
export declare const senseless_channel_group_permissions: PermissionType[];
export declare const senseless_channel_permissions: PermissionType[];
export declare const senseless_client_permissions: PermissionType[];
export declare const senseless_client_channel_permissions: PermissionType[];
