import { ConnectionHandler } from "tc-shared/ConnectionHandler";
import { DefaultTabValues } from "tc-shared/ui/modal/permission/ModalRenderer";
import { PermissionEditorTab } from "tc-shared/ui/modal/permission/ModalDefinitions";
export declare function spawnPermissionEditorModal(connection: ConnectionHandler, defaultTab?: PermissionEditorTab, defaultTabValues?: DefaultTabValues): void;
