import { ConnectionHandler } from "../../ConnectionHandler";
export declare function openBanList(client: ConnectionHandler): void;
export declare const duration_data: {
    sec: {
        text: string;
        "1-text": string;
        scale: number;
    };
    min: {
        text: string;
        "1-text": string;
        scale: number;
    };
    hours: {
        text: string;
        "1-text": string;
        scale: number;
    };
    days: {
        text: string;
        "1-text": string;
        scale: number;
    };
};
