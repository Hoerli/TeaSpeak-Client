import { ConnectionHandler } from "../../ConnectionHandler";
export declare function spawnIconSelect(client: ConnectionHandler, callback_icon?: (id: number) => any, selected_icon?: number): void;
export declare function spawnIconUpload(client: ConnectionHandler): void;
