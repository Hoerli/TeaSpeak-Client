/**
 * Ensure that the module has been loaded within the main application and not
 * within a popout.
 */
export declare function assertMainApplication(): void;
