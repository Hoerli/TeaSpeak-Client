/// <reference types="react" />
export declare const Arrow: (props: {
    direction: "up" | "down" | "left" | "right";
    className?: string;
    onClick?: () => void;
}) => JSX.Element;
