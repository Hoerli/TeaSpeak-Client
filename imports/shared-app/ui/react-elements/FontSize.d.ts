import * as React from "react";
export declare const FontSizeObserver: React.MemoExoticComponent<(props: {
    children: (fontSize: number) => React.ReactNode | React.ReactNode[];
}) => JSX.Element>;
