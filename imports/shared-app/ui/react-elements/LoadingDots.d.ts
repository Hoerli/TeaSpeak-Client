/// <reference types="react" />
export declare const LoadingDots: (props: {
    maxDots?: number;
    speed?: number;
    textOnly?: boolean;
    enabled?: boolean;
}) => JSX.Element;
