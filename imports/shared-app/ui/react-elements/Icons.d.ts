/// <reference types="react" />
import { ClientIcon } from "svg-sprites/client-icons";
export declare const ClientIconRenderer: (props: {
    icon: ClientIcon;
    size?: string | number;
    title?: string;
    className?: string;
}) => JSX.Element;
