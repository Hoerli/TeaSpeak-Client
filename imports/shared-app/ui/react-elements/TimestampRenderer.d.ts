/// <reference types="react" />
export declare const TimestampRenderer: (props: {
    timestamp: number;
}) => JSX.Element;
