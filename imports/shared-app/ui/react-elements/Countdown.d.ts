import * as React from "react";
export declare const Countdown: (props: {
    timestamp: number;
    finished?: React.ReactNode;
    callbackFinished?: () => void;
}) => JSX.Element;
