/// <reference types="react" />
export declare const CountryCode: (props: {
    alphaCode: string;
    className?: string;
}) => JSX.Element;
