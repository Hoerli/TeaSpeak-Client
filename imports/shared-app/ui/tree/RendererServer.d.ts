import * as React from "react";
import { RDPServer } from "tc-shared/ui/tree/RendererDataProvider";
export declare class ServerRenderer extends React.Component<{
    server: RDPServer;
}, {}> {
    render(): JSX.Element;
}
