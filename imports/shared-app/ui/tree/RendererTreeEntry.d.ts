import * as React from "react";
import { RDPEntry } from "tc-shared/ui/tree/RendererDataProvider";
export declare class UnreadMarkerRenderer extends React.Component<{
    entry: RDPEntry;
}, {}> {
    render(): JSX.Element;
}
