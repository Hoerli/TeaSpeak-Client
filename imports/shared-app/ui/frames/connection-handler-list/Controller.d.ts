import { Registry } from "tc-shared/events";
import { ConnectionListUIEvents } from "tc-shared/ui/frames/connection-handler-list/Definitions";
export declare function initializeConnectionListController(events: Registry<ConnectionListUIEvents>): void;
