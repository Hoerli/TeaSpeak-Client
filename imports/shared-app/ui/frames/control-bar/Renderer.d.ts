/// <reference types="react" />
import { Registry } from "tc-shared/events";
import { ControlBarEvents } from "tc-shared/ui/frames/control-bar/Definitions";
export declare const ControlBar2: (props: {
    events: Registry<ControlBarEvents>;
    className?: string;
}) => JSX.Element;
