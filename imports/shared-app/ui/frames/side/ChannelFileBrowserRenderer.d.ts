/// <reference types="react" />
import { Registry } from "tc-shared/events";
import { ChannelFileBrowserUiEvents } from "tc-shared/ui/frames/side/ChannelFileBrowserDefinitions";
export declare const ChannelFileBrowser: (props: {
    events: Registry<ChannelFileBrowserUiEvents>;
}) => JSX.Element;
