import React from "react";
export declare const ImagePreviewHook: React.MemoExoticComponent<() => JSX.Element>;
export declare function showImagePreview(url: string, originalUrl: string): void;
export declare function isImagePreviewAvailable(): boolean;
export declare function closeImagePreview(): void;
